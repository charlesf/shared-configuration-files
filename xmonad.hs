import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.NoBorders
import XMonad.Layout.Gaps
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import System.IO

main = do
    xmproc <- spawnPipe "xmobar /home/charles/.xmonad/xmobarrc"

    xmonad $ defaultConfig
        { manageHook = manageDocks <+> manageHook defaultConfig
        , layoutHook = avoidStruts $ layoutHook defaultConfig
        , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
                        }
	, terminal = "st"
        , modMask = mod4Mask     -- Rebind Mod to the Windows key
        } `additionalKeys`
        [ ((mod4Mask .|. shiftMask, xK_z), spawn "xscreensaver-command -lock; xset dpms force off")
        , ((controlMask, xK_Print), spawn "sleep 0.2; scrot -se 'mv $f ~/Pictures/Screenshots'")
        , ((0, xK_Print), spawn "scrot -e 'mv $f ~/Pictures/Screenshots'")
	, ((0, 0x1008ff13),     spawn "amixer -q set Master 10%+") --raise sound
        , ((0, 0x1008ff11),     spawn "amixer -q set Master 5%-") --lower sound
        , ((0, 0x1008ff12),     spawn "amixer -q set Master toggle") --mute sound
        , ((0, 0x1008ff03),     spawn "xbacklight -dec 5") --decrease brightness
        , ((0, 0x1008ff02),     spawn "xbacklight -inc 10") --increase brightness
        ]
