# -*- mode: sh -*-

# Commands that should be applied only for interactive shells.
if [[ $- == *i* ]]; then
 HISTFILE="$HOME/.bash_history"
 HISTFILESIZE=100000
 HISTIGNORE=ls:cd:exit
 HISTSIZE=100000
  
 shopt -s histappend
 shopt -s checkwinsize
 shopt -s extglob
 shopt -s globstar
 shopt -s checkjobs


 export VISUAL=nvim
 export EDITOR="$VISUAL"
 alias eta="exiftool -All="
fi


