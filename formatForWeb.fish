function formatForWeb
    for i in $argv
        set link = (curl -F file=@$i https://api.put.re/upload | jq '.data.link')
        echo "<img>$link</img>" >> weblinks.txt
    end
end
