set backspace=2         " backspace in insert mode works like normal editor
syntax on               " syntax highlighting
filetype indent on      " activates indenting for files
set autoindent          " auto indenting
set number              " line numbers
set ruler		" shows line,column and percentage of buffer
colorscheme desert      " colorscheme desert
set nobackup            " get rid of anoying ~file
set ww=<,>,h,l		" lets you move to lines above/below with h,l
set shm=at 		" shortens all messages, so as to avoid scrolling
set nosol		" cursor no longer changes columns when changing rows
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8
set nocompatible	" disable vi compatibility (emulation of old bugs)
set smartindent		" intelligent indent for C
set tabstop=4 		" tab width is 4 spaces
set shiftwidth=4  	" indent also with 4 spaces
set expandtab 		" expands tabs into spaces
set textwidth=120	" 80 is too small nowadays
set showmatch		" intelligent comments

set tags+=~/.vim/tags/cpp
set tags+=~/.vim/tags/gl
set tags+=~/.vim/tags/sdl
set tags+=~/.vim/tags/qt4

" in normal mode F2 will save the file
nmap <F2> :w<CR>
" in insert mode F2 will exit insert, save, enters insert again
imap <F2> <ESC>:w<CR>i

" in normal mode F7 and F8 tab to previous and next tabs respectively
nmap <F7> :tabp<CR>
nmap <F8> :tabn<CR>

" map : to ;
nmap ; :

" in insert mode F7 and F8 tab to previous and next tabs respectively
imap <F7> <ESC> :tabp<CR>i
imap <F8> <ESC> :tabn<CR>i


" enable autoclosing of various types of pairs (DOES NOT WORK in paste mode)
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O
inoremap jj <ESC>
