{ config, pkgs, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  xsession.enable = true;
  programs.home-manager.path = https://github.com/rycee/home-manager/archive/master.tar.gz;
  # xsession.windowManager.command = "...";

  services.redshift = {
    enable = true;
    latitude = "42.3736";
    longitude = "71.1097";
    brightness.day = "1";
    brightness.night = "1";
    tray = true;
  };

  programs.git = {
    enable = true;
    userName  = "Charles Freeman";
    userEmail = "charleswfreeman@protonmail.com";
  };

  services.udiskie.enable = true;

  programs.neovim = {
    enable = true;
    viAlias = true;
    vimAlias = true;
  };

#  programs.bash = {
#    enable = true;
#    historySize = 100000;
#    initExtra = "export VISUAL=nvim \n export EDITOR=\"$VISUAL\" \n alias eta=\"exiftool -All=\"";
#    historyIgnore = ["ls" "cd" "exit"];
#  };

  # let
  #   xmonad = pkgs.xmonad-with-packages.override {
  #     packages = self: [ self.xmonad-contrib self.taffybar ];
  #   };
  # in
  #   "${xmonad}/bin/xmonad";

  xsession.windowManager.xmonad = {
    enable = true;
    enableContribAndExtras = true;
  };
}
